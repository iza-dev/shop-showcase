User-agent: *
Disallow: /admin/
Disallow: /api/
Sitemap: https://shop-showcase.vercel.app/sitemap.xml

import { Box, Heading, Image, Text, useTheme } from '@chakra-ui/react';
import { FC } from 'react';

import { ArticleCard } from '../components/ArticleCard';
import { ListG } from '../components/List';

import ginger from '../assets/images/products/ginger.png';
import soup from '../assets/images/products/soup.jpeg';

export const ArticlePage: FC = () => {
  const theme = useTheme();

  return (
    <Box position="relative">
      <Box
        position="absolute"
        top={0}
        left={0}
        width="100%"
        height="70vh"
        backgroundColor={theme.colors.green_anis}
        zIndex={-1}
      />
      <Box zIndex={10} marginX={'auto'} width={'70vmin'} textAlign={'center'} paddingTop={'5rem'}>
        <Heading as={'h1'} color={theme.colors.gobios} mb={'2rem'}>
          <Box
            as={'span'}
            display="block"
            fontSize={{ base: '4xl', md: '5xl', lg: '6xl' }}
            letterSpacing={{ base: '0', md: '0.3rem', lg: '0.5rem' }}
          >
            Vitalité et Fraîcheur
          </Box>
          <br />
          La bière de Gingembre Bio qui éveille vos papilles
        </Heading>
        <Image
          maxW="95vmin"
          marginLeft={'-12.5vmin'}
          marginY={'5rem'}
          src={ginger}
          alt={'image article'}
        />
        <Text
          fontFamily={theme.fonts.p_intro}
          fontStyle={'italic'}
          fontWeight={'medium'}
          fontSize="xl"
          mb="4rem"
        >
          #sante #ocomtoisgourmand #apero #gingembre #bio
        </Text>

        <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
          Découvrez le plaisir rafraîchissant du jus de gingembre BIO Ginger et Ginger. Désormais en
          vente dans votre boutique d' O Comtois Gourmand.
        </Text>
        <Text fontSize="lg" mb="4rem" textAlign={'justify'}>
          Ce trésor biologique, élaboré avec passion à Nantes. Incarne la quintessence de la
          production régionale française, promettant bien-être et éveil des sens. Avec 40% de
          gingembre, chaque gorgée est une invitation à revitaliser votre quotidien. Que ce soit
          pour dynamiser vos matinées ou agrémenter vos soirées, Ginger et Ginger transforme
          l'ordinaire en extraordinaire. <br /> Sa formule faiblement sucrée, et sans alcool,
          accompagné de notre délicieuse eau pétillante de Velleminfroy. Vous permettrons de créer
          des cocktails riches en saveurs. Disponible en trois variantes – Gingembre pur, Gingembre
          & Curcuma, et Gingembre & Hibiscus. ginger-ginger offre une touche culinaire unique, à vos
          plats et boissons. Faites l'expérience de ce concentré de vitalité, symbole d'un mode de
          vie sain et gourmand.
        </Text>
        <Box>
          <Heading as={'h3'}>Découvrez nos autres actualités</Heading>
          <ListG
            pt={'3rem'}
            width={'100%'}
            direction={{ base: 'column', md: 'row' }}
            margin={{ md: 'auto', lg: 0 }}
            gap={'1rem'}
            // TODO add real datas

            items={[
              {
                id: 1,
                name: 'Item 1',
                title: "Savourez nos Soupes d'été artisanales",
                description: "Fraîcheur et saveurs locales à l'honneur!",
                image: soup,
                url: ''
              }
            ]}
            renderItem={item => (
              <ArticleCard
                minHeight={{ base: 'md', md: 'xs', lg: 'md' }}
                maxWidth={{ base: '1xs', md: '2xs', lg: 'xs' }}
                textAlign={'center'}
                data={item}
                url="/articles/1"
                image={
                  <Image
                    width={{ base: 'xs', md: 'md' }}
                    margin={'auto'}
                    src={item.image}
                    alt={'image article'}
                    borderRadius={'lg'}
                  />
                }
              />
            )}
          />
        </Box>
      </Box>
    </Box>
  );
};

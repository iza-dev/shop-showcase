import { Box, Divider, Flex, Heading, Image, Text } from '@chakra-ui/react';
import { FC } from 'react';
import { ArticleCard, ArticleCardWithBadge } from '../components/ArticleCard';
import { BigPicture } from '../components/BigPicture';
import { ListG } from '../components/List';

import banner from '../assets/images/banner_long.png';
import vege from '../assets/images/inside_vege.jpeg';
import asperg from '../assets/images/products/asperg.jpg';
import carrot from '../assets/images/products/carrot.png';
import ginger from '../assets/images/products/ginger.png';
import potato from '../assets/images/products/potato.png';
import wine from '../assets/images/products/wine.jpg';

export const HomePage: FC = () => {
  return (
    <Box>
      <BigPicture />
      <Box width={{ md: 'container.sm', lg: 'container.lg' }} marginX={'auto'}>
        <Flex
          minHeight={'95vh'}
          aria-label="section nos dernières actualités"
          direction={'column'}
          padding={'2rem'}
          as={'section'}
        >
          <Flex
            direction={{ base: 'column', md: 'row' }}
            justifyContent={'space-between'}
            marginBottom={'2rem'}
            gap={'1rem'}
          >
            <Heading as={'h1'}>Nos dernières actualités</Heading>
            <Text maxW={'md'} textAlign={'justify'}>
              Venez découvrir nos nouveaux produits locaux, frais et de saison. Profitez également
              de nos offres spéciales sur une sélection d'articles. Nous avons récemment élargi
              notre gamme de produits biologiques et artisanaux pour répondre à toutes vos envies
              gourmandes. De plus, notre équipe est ravie de vous accueillir dans un environnement
              sûr et convivial. Restez à l'écoute pour plus de surprises à venir!
            </Text>
          </Flex>
          <ListG
            width={'100%'}
            direction={{ base: 'column', md: 'row' }}
            margin={{ md: 'auto', lg: 0 }}
            gap={'1rem'}
            // TODO add real datas

            items={[
              {
                id: 1,
                name: 'Item 1',
                description: 'La boisson sans alcool au gingembre Bio qui éveille vos papilles',
                title: 'Vitalité et Fraîcheur',
                image: ginger,
                url: ''
              }
            ]}
            renderItem={item => (
              <ArticleCard
                minHeight={{ base: 'md', md: 'xs', lg: 'md' }}
                maxWidth={{ base: '1xs', md: '2xs', lg: 'md' }}
                textAlign={'center'}
                data={item}
                url="/articles/1"
                image={
                  <Image
                    width={{ base: 'xs', md: 'md' }}
                    margin={'auto'}
                    src={item.image}
                    alt={'image article'}
                    borderRadius={'lg'}
                  />
                }
              />
            )}
          />
        </Flex>

        <Divider w={'50vw'} marginX={'auto'} />

        <Flex
          minHeight={'95vh'}
          aria-label={'section des top produits'}
          as={'section'}
          direction={'column'}
          padding={'2rem'}
        >
          <Flex
            gap={'1rem'}
            justifyContent={'space-between'}
            marginBottom={'7rem'}
            direction={{ base: 'column', md: 'row' }}
          >
            <Heading as={'h1'}>On mange quoi en ce moment?</Heading>
            <Text maxW={'md'} textAlign={'justify'}>
              En cette saison d'hiver, on vous fait découvrir les bons plans gourmands et régionaux!
            </Text>
          </Flex>
          <ListG
            direction={{ base: 'column', md: 'row' }}
            margin={{ md: 'auto', lg: 0 }}
            justifyContent={'flex-end'}
            gap={'5rem'}
            // TODO add real datas
            items={[
              { id: 1, title: 'Asperges', description: 'Description for Item 1', image: asperg },
              { id: 2, title: 'Vin Rosé', description: 'Description for Item 2', image: wine }
            ]}
            renderItem={item => (
              <ArticleCardWithBadge
                minHeight={{ base: 'md', md: 'xs' }}
                textAlign={'center'}
                data={item}
                badgeColorScheme="red"
                image={
                  <Image
                    maxWidth={'1xs'}
                    margin={'auto'}
                    src={item.image}
                    alt={'image article'}
                    borderRadius={'lg'}
                  />
                }
              />
            )}
          />
        </Flex>

        <Divider w={'50vw'} marginX={'auto'} />

        <Flex
          minHeight={'95vh'}
          aria-label="section engagement contre l'inflation"
          direction={'column'}
          padding={'2rem'}
          as={'section'}
          textAlign={{ md: 'center' }}
          gap={'2rem'}
        >
          <Heading as={'h1'}>Engagement contre l'inflation</Heading>
          <Image maxWidth={'5rem'} src={carrot} marginX={'auto'} marginY={'2rem'} alt="carottes" />
          <Text maxW={'md'} textAlign={'justify'} marginX={'auto'}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, amet ea iusto temporibus
            nesciunt dignissimos placeat. Provident, fugit ut neque earum error ipsa doloribus, unde
            voluptate quibusdam numquam, odio explicabo ducimus nesciunt magnam ex cumque beatae
            reprehenderit amet dolore nisi dolor accusantium. Ea corporis fugiat doloribus dolorum
            eveniet, inventore accusantium corrupti sapiente tempore mollitia, dignissimos cumque
            dolorem temporibus. Officia, eveniet voluptate! Temporibus pariatur, laborum ipsam
            dolorum ducimus quam obcaecati eaque dolorem! Quibusdam maxime praesentium consectetur
            numquam quia distinctio, tempora maiores sit aut aperiam ex ipsum nulla porro ut ab
            temporibus, fuga qui nostrum obcaecati mollitia veniam necessitatibus facilis. Iure,
            non?
          </Text>
          <Image
            maxWidth={'5rem'}
            src={potato}
            marginX={'auto'}
            marginY={'2rem'}
            alt="pommes de terre"
          />
        </Flex>

        <Divider w={'50vw'} marginX={'auto'} />

        <Flex
          minHeight={'95vh'}
          aria-label="section travaillez avec nous"
          as={'section'}
          direction={'column'}
          padding={'2rem'}
        >
          <Image
            src={banner}
            marginY={{ base: '2rem', md: '8rem', lg: 'auto' }}
            alt={'image de bannière'}
          />

          <Flex direction={{ base: 'column', md: 'column', lg: 'row' }} gap={'4rem'}>
            <Text maxW={'md'} textAlign={'justify'} marginX={{ base: 'auto', md: 'auto' }}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, amet ea iusto temporibus
              nesciunt dignissimos placeat. Provident, fugit ut neque earum error ipsa doloribus,
              unde voluptate quibusdam numquam, odio explicabo ducimus nesciunt magnam ex cumque
              beatae reprehenderit amet dolore nisi dolor accusantium. Ea corporis fugiat doloribus
              dolorum eveniet. Iure, non?
            </Text>
            <Image
              maxWidth={{ base: 'sx', md: 'md', lg: 'lg' }}
              src={vege}
              marginX={'auto'}
              borderRadius={'lg'}
              alt="image intérieur du magasin"
            />
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
};

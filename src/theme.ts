import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
    colors: {
        green_anis: "#cccc99",
        brown_dark: "#2a1914"
    },
    fonts: {
        heading: "'Cormorant Garamond', serif",
        body: "'Montserrat Alternates', sans-serif",

    }
});

export default theme;

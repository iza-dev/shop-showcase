import { createBrowserRouter } from 'react-router-dom';
import App from './App';
import { HomePage } from './pages';
import { ArticlePage } from './pages/ArticlePage';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/',
        element: <HomePage />
      },
      {
        path: '/articles/1',
        element: <ArticlePage />
      }
    ]
  }
]);

import { Flex, Icon, IconButton } from '@chakra-ui/react';
import { FC, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';

import { FaBars } from 'react-icons/fa';

import { Logo } from './Logo';
import { NavMenu } from './NavMenu';

const Navbar: FC = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [showBurger, setShowBurger] = useState(true);
  const toggleMenu = () => {
    setShowMenu(!showMenu);
    setShowBurger(!showBurger);
  };

  return (
    <>
      {isMobileOnly && (
        <IconButton
          zIndex={9999}
          top={'2.5rem'}
          right={'2.5rem'}
          position={'relative'}
          float={'right'}
          aria-label="Menu"
          icon={<Icon as={FaBars} />}
          display={{ base: 'flex' }}
          onClick={toggleMenu}
        />
      )}
      <Flex p="4" color="white" as={'nav'} alignItems={'center'} justifyContent={'space-around'}>
        <Logo />

        {!isMobileOnly && <NavMenu />}
        {isMobileOnly && showMenu && <NavMenu direction={showMenu ? 'column' : 'row'} />}
      </Flex>
    </>
  );
};

export default Navbar;

import { Box, Heading, Image, Text, useTheme } from '@chakra-ui/react';
import { FC } from 'react';

import boss_picture from '../assets/images/epicier.png';
import GlobalSearch from './GlobalSearch';

export const BigPicture: FC = () => {
  const theme = useTheme();
  return (
    <Box
      position="relative"
      height={{ base: '104vh', md: '105vh' }}
      backgroundColor={theme.colors.green_anis}
    >
      <Heading
        fontFamily={theme.fonts.body}
        paddingTop={{ base: '2rem', md: '4rem', lg: '4rem' }}
        as={'h1'}
        marginLeft={{ base: '5%', md: '15%', lg: '2xl' }}
        marginBottom={'3rem'}
        maxWidth={{ base: 'sx', md: 'md', lg: 'lg' }}
      >
        Découvrez une expérience gustative unique 🍽️
      </Heading>
      <Text
        marginLeft={{ base: '5%', md: '15%', lg: '2xl' }}
        textAlign={'justify'}
        maxWidth={{ base: 'sx', md: 'md', lg: 'lg' }}
        display={{ base: 'none', md: 'block' }}
      >
        Laissez Bruno et Stéphanie vous guider 🛒. Nous sommes des experts passionnés, là pour vous
        accompagner, à travers une sélection minutieuse de produits locaux et régionaux 🌱. Nous
        vous offrons des conseils personnalisés selon vos goûts, et vos besoins 😊 pour une
        expérience culinaire sur mesure. 🍴
      </Text>
      <GlobalSearch
        position="absolute"
        top={{ base: '25%', md: '45%', lg: '55%' }}
        minW={{ base: 'sx', md: 'sx', lg: 'md' }}
        left={{ base: '10%', md: '15%', lg: '2xl' }}
        onSearch={() => console.log('hello')}
      />
      <Image
        position="absolute"
        bottom={0}
        right={0}
        maxW={{ base: 'xs', md: 'md' }}
        objectFit="cover"
        src={boss_picture}
        alt="épicier qui conseille les clients"
      />
    </Box>
  );
};

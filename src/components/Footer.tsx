import { Divider, Flex, FlexProps, Text, useTheme } from '@chakra-ui/react';
import { FC } from 'react';
import { Logo } from './Logo';
import { SocialNetwork } from './SocialNetwork';

export const Footer: FC<FlexProps> = ({ ...rest }) => {
  const theme = useTheme();
  return (
    <Flex
      {...rest}
      p={'1rem'}
      direction={'column'}
      backgroundColor={theme.colors.green_anis}
      marginTop={{ base: '5rem', md: '8rem', lg: '10rem' }}
    >
      <Flex justifyContent={'space-around'} direction={{ base: 'column', md: 'row' }} gap={'2rem'}>
        <Flex direction={'column'} alignItems={'center'}>
          <Logo />
          <SocialNetwork />
        </Flex>
        <Flex direction={'column'}>
          <Text fontWeight={'bold'}>Contactez-nous</Text>
          <Text>03 81 50 82 14</Text>
          <Text>06 49 23 38 34</Text>
          <Text>bruno.pertuis@hotmail.fr</Text>
        </Flex>
        <Flex direction={'column'}>
          <Text fontWeight={'bold'}>Horaires</Text>
          <Text>Du mardi au samedi</Text>
          <Text>9h00-12h30 et 15h00-19h30</Text>
          <Text>Le dimanche 8h30 - 12h00</Text>

          <Text>8 Rue du Lavoir 25480 Pirey</Text>
        </Flex>
      </Flex>
      <Divider marginY={'1rem'} />
      <Flex gap={'2rem'} flexWrap="wrap" marginX={'auto'} p={'2rem'}>
        <Text flexShrink={0}>Mentions légales</Text>
        <Text flexShrink={0}>Gestion des cookies</Text>
        <Text flexShrink={0}>CG Programme de Fidélité</Text>
        <Text flexShrink={0}>copyright© 2024</Text>
      </Flex>
    </Flex>
  );
};

import { Box, Button, Heading, Image } from '@chakra-ui/react';
import { NavLink } from 'react-router-dom';
import logo from '../assets/images/logo.png';

export const Logo = () => {
  return (
    <Box p="2">
      <NavLink to={'/'}>
        <Button variant="link" mr="4" marginX={'auto'}>
          <Image flexShrink={0} src={logo} width={{ base: '8rem', md: '15rem' }} alt="logo" />
          <Heading display={'none'} as={'h1'} size="md">
            O Comtois Gourmand
          </Heading>
        </Button>
      </NavLink>
    </Box>
  );
};

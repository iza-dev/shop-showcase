import { Flex, IconButton, useTheme } from '@chakra-ui/react';
import { FC } from 'react';
import { FaFacebook, FaInstagram } from 'react-icons/fa';
import { Link } from 'react-router-dom';

export const SocialNetwork: FC = () => {
  const theme = useTheme();
  return (
    <Flex gap={'1rem'} p={'1rem'}>
      <Link to={'https://www.instagram.com/ocomtoisgourmand/'}>
        <IconButton
          width={'1rem'}
          bg={theme.colors.brown_dark}
          color={'white'}
          icon={<FaInstagram size={'100%'} />}
          aria-label={'lien instagram'}
        />
      </Link>
      <Link to={'https://www.facebook.com/ocomtoisgourmand'}>
        <IconButton
          width={'1rem'}
          bg={theme.colors.brown_dark}
          color={'white'}
          icon={<FaFacebook size={'100%'} />}
          aria-label={'lien instagram'}
        />
      </Link>
    </Flex>
  );
};

import {
  Badge,
  Box,
  BoxProps,
  Button,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Heading,
  Image,
  Text
} from '@chakra-ui/react';
import { FC } from 'react';
import { FaLongArrowAltRight } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import promo from '../assets/badge_promo.png';

type Article = {
  title: string;
  description: string;
  image: string;
};

type ArticleCardProps<T extends boolean> = {
  url?: string;
  data: Article;
  image: JSX.Element;
} & (T extends true ? { badgeColorScheme: string } : { badgeColorScheme?: never }) &
  BoxProps;

export const ArticleCard: FC<ArticleCardProps<false>> = ({ data, image, url, ...rest }) => {
  console.log('url ', url);

  return (
    <Card {...rest}>
      <CardHeader>
        <Flex position={'relative'}>{image}</Flex>
      </CardHeader>
      <CardBody display="flex" flexDirection="column">
        <Heading as="h4" fontSize={{ base: '2xl', md: 'lg', lg: '2xl' }} mb={'0.5rem'}>
          {data.title}
        </Heading>
        <Text flex="1">{data.description}</Text>
        <Link to={url || '#'}>
          <Button
            aria-label="bouton pour lire la suite d'un article"
            rightIcon={<FaLongArrowAltRight />}
          >
            Lire la suite
          </Button>
        </Link>
      </CardBody>
    </Card>
  );
};

export const ArticleCardWithBadge: FC<ArticleCardProps<true>> = ({
  data,
  image,
  badgeColorScheme,
  url,
  ...rest
}) => {
  return (
    <Card {...rest}>
      <CardHeader>
        <Flex position={'relative'}>
          {image}
          <Box position="absolute" top="-10" right="-10" zIndex="10">
            <Badge
              colorScheme={badgeColorScheme}
              borderRadius="full"
              p="1"
              position="relative"
              display="flex"
            >
              <Image
                width={70}
                height={70}
                src={promo}
                alt={`Badge for promotion`}
                borderRadius="full"
              />
            </Badge>
          </Box>
        </Flex>
      </CardHeader>
      <CardBody display="flex" flexDirection="column">
        <Heading as="h4" fontSize={{ base: '2xl', md: 'lg', lg: '2xl' }} mb={'0.5rem'}>
          {data.title}
        </Heading>
        <Text flex="1">{data.description}</Text>
        <Link to={url || '#'}>
          <Button
            aria-label="bouton pour lire la suite d'un article"
            rightIcon={<FaLongArrowAltRight />}
          >
            Lire la suite
          </Button>
        </Link>
      </CardBody>
    </Card>
  );
};

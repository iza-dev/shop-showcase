import { Button, Flex, FlexProps, useTheme } from '@chakra-ui/react';
import { FC } from 'react';
import { NavLink } from 'react-router-dom';

export const NavMenu: FC<FlexProps> = ({ ...rest }) => {
  const theme = useTheme();
  return (
    <Flex as={'ul'} {...rest}>
      <NavLink to={'/'}>
        <Button color={theme.colors.brown_dark} variant="link" mr="4">
          Notre histoire
        </Button>
      </NavLink>
      <NavLink to={'/'}>
        <Button color={theme.colors.brown_dark} variant="link" mr="4">
          Engagements
        </Button>
      </NavLink>
      <NavLink to={'/'}>
        <Button color={theme.colors.brown_dark} variant="link" mr="4">
          Produits
        </Button>
      </NavLink>
      <NavLink to={'/'}>
        <Button color={theme.colors.brown_dark} variant="link" mr="4">
          Articles
        </Button>
      </NavLink>
    </Flex>
  );
};

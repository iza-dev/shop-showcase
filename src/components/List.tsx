import { Box, Flex, FlexProps, ListItem } from '@chakra-ui/react';

type ListItem = {
  id: number;
  name?: string;
};

type ListProps<T extends ListItem> = {
  items: T[];
  renderItem: (item: T) => JSX.Element;
} & FlexProps;

export const ListG = <T extends ListItem>({ items, renderItem, ...rest }: ListProps<T>) => {
  return (
    <Flex direction={{ base: 'column', lg: 'row' }} {...rest}>
      {items.map(item => (
        <Box key={item.id} flexBasis={{ base: '100%', lg: 'auto' }}>
          {renderItem(item)}
        </Box>
      ))}
    </Flex>
  );
};

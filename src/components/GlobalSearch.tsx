import { Box, BoxProps, IconButton, Input, InputGroup, InputRightElement } from '@chakra-ui/react';
import React, { useState } from 'react';
import { CiSearch } from 'react-icons/ci';

type GlobalSearchProps = {
  onSearch: (query: string) => void;
} & BoxProps;

const GlobalSearch: React.FC<GlobalSearchProps> = ({ onSearch, ...rest }) => {
  const [searchQuery, setSearchQuery] = useState<string>('');

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    onSearch(searchQuery);
  };

  return (
    <Box {...rest}>
      <form onSubmit={handleSubmit}>
        <InputGroup>
          <Input
            type="text"
            placeholder="Trouver un article..."
            value={searchQuery}
            onChange={handleInputChange}
            borderColor={'black'}
            _placeholder={{ color: 'black', fontWeight: 'bold' }}
          />
          <InputRightElement>
            <IconButton
              backgroundColor={'black'}
              aria-label="Search"
              icon={<CiSearch color="white" />}
              onClick={() => onSearch(searchQuery)}
            />
          </InputRightElement>
        </InputGroup>
      </form>
      {/* 
      <List mt={2}>
        <ListItem>Search Result 1</ListItem>
        <ListItem>Search Result 2</ListItem>
        <ListItem>Search Result 3</ListItem>
      </List> */}
    </Box>
  );
};

export default GlobalSearch;

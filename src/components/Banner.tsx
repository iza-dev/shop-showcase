import { Flex, FlexProps, Text } from '@chakra-ui/react';
import { FC } from 'react';

export const Banner: FC<FlexProps> = ({ ...rest }) => {
  return (
    <Flex {...rest}>
      <Flex
        gap={{ base: '0.5rem', md: ' 2rem' }}
        alignItems={'center'}
        direction={{ base: 'column', md: 'row' }}
      >
        <Text
          textAlign={{ base: 'justify', md: 'start' }}
          fontSize={{ base: 'xs', md: 'xs', lg: 'xs' }}
        >
          Votre épicerie met à l'honneur le terroir franc-comtois, célébrant l'authenticité de
          l'artisanat local et des spécialités régionales, le tout en circuit court.
        </Text>
      </Flex>
    </Flex>
  );
};

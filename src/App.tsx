import { useTheme } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';
import './App.css';
import { Banner } from './components/Banner';
import { Footer } from './components/Footer';
import Navbar from './components/Navbar';

function App() {
  const theme = useTheme();
  return (
    <>
      <Banner
        p={'1rem'}
        gap={{ base: '0.5rem', md: ' 3rem' }}
        backgroundColor={theme.colors.brown_dark}
        color={'white'}
        alignItems={'center'}
        direction={{ base: 'column', md: 'row' }}
      />
      <Navbar />
      <Outlet />
      <Footer />
    </>
  );
}

export default App;
